import { UserListComponent } from './user-list/user-list.component';
import { CreateTurnComponent } from './create-turn/create-turn.component';
import { CreateWorkspaceComponent } from './create-workspace/create-workspace.component';
import { TurnComponent } from './turn/turn.component';
import { FrontendRoutingModule } from './frontend-routing.module';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    FrontendRoutingModule
  ],
  declarations: [LoginComponent, MainComponent, TurnComponent, CreateWorkspaceComponent, CreateTurnComponent, UserListComponent],
  exports: [MainComponent]
})
export class FrontendModule { }
