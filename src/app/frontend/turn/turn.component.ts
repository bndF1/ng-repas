import { Component, OnInit } from '@angular/core';
import { TurnsService } from '@app/core';
import { ITurn } from '@app/core/interfaces';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-turn',
  templateUrl: './turn.component.html',
  styleUrls: ['./turn.component.scss']
})
export class TurnComponent implements OnInit {
  turnList: Observable<ITurn>;

  constructor(private turnService: TurnsService) {}

  ngOnInit() {
    this.turnList = this.turnService.getAllTurns();
  }
}
