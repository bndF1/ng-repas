import { CreateTurnComponent } from './create-turn/create-turn.component';
import { CreateWorkspaceComponent } from './create-workspace/create-workspace.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TurnComponent } from './turn/turn.component';
import { UserListComponent } from './user-list/user-list.component';
import { AuthGuardGuard } from '@app/core/guards/auth/auth-guard.guard';

const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuardGuard],
    children: [
      { path: '', component: TurnComponent },
      { path: 'turn', component: TurnComponent },
      { path: 'create-turn', component: CreateTurnComponent },
      { path: 'user-list', component: UserListComponent }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FrontendRoutingModule {}
