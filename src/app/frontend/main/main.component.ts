import { Router } from '@angular/router';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from '@app/core';
import { TdMediaService } from '@covalent/core/media';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {

  user;

  constructor(private _changeDetectorRef: ChangeDetectorRef, public media: TdMediaService,
    private authenticationService: AuthenticationService, private _iconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer, private router: Router) { }

  ngOnInit() {
    this._iconRegistry.addSvgIconInNamespace('assets', 'usercap',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/usercap.svg'));

    this.authenticationService.user.subscribe(user => {
      console.log(user);
      this.user = user;
    });
  }


  ngAfterViewInit(): void {
    // broadcast to all listener observables when loading the page
    this.media.broadcast();
    // force a new change detection cycle since change detections
    // have finished when `ngAfterViewInit` is executed
    this._changeDetectorRef.detectChanges();
  }

  logOut() {
    this.authenticationService.signOut().then((result) => {
      this.router.navigate(['login']);
    });
  }
}
