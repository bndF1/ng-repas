import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { FIREBASE_CONFIG } from './firebase.credentials';


@NgModule({
  imports: [
    AngularFireModule.initializeApp(FIREBASE_CONFIG), // imports firebase/app needed for everything
  ],
  declarations: []
})
export class ConfigModule { }
