import { DocumentReference } from '../../../node_modules/angularfire2/firestore';

export interface ITurn {
  id: string;
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  capacity: number;
  attendees: Array<DocumentReference>;
  teacherRef: DocumentReference;
}

export interface IUser {
  id: string;
  email: string;
  name: string;
  surname: string;
  isTeacher: boolean;
  grade: IGrade;
  workspace: Array<DocumentReference>;
}

export interface IGrade {
  id: string;
  name: string;
  description: string;
}

export interface IWorkspace {
  id: string;
  name: string;
  description: string;
  private: boolean;
  url: string;
  ownerId: string;
  managers: Array<string>;
}
