import { Injectable } from '@angular/core';
import { User } from '@firebase/auth-types';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TdDialogService } from '@covalent/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  user: Observable<User>;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private dialogService: TdDialogService) {
    this.user = this.afAuth.authState.pipe(
      switchMap((user) => {
        const myUser = user
          ? this.afs.doc<User>(`users/${user.uid}`).valueChanges()
          : of(null);
        return myUser;
      })
    );
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        return this.getUser(user);
      })
      .catch(error => {
        return this.handleError(error);
      });
  }

  signOut(): Promise<any> {
    return this.afAuth.auth.signOut();
  }

  private handleError(error) {
    console.error(error);
    return this.dialogService.openAlert({
      message: error,
      title: error,
    });
  }

  private getUser(user) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);
    return userRef;
  }
}
