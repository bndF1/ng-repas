import { AuthenticationService } from './authentication/authentication.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuardGuard } from './guards/auth/auth-guard.guard';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [AuthenticationService, AuthGuardGuard],
  declarations: []
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
