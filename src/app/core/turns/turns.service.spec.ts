import { TestBed, inject } from '@angular/core/testing';

import { TurnsService } from './turns.service';

describe('TurnsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TurnsService]
    });
  });

  it('should be created', inject([TurnsService], (service: TurnsService) => {
    expect(service).toBeTruthy();
  }));
});
