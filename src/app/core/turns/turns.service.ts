import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TurnsService {

  constructor(public afs: AngularFirestore) { }



  getAllTurns(): Observable<any> {
    return this.afs.collection('turn').valueChanges();
  }

  getTurnById(turnId: string): Observable<any> {
    return this.afs.collection('turn').doc(`${turnId}`).valueChanges();
  }

  getTurnByTeacherId(teacherId: string): Observable<any> {
    // return this.afs.collection('turn', ref => ref.where('teacherId', '==', teacherId).orderBy('startDate')).valueChanges();
    return this.getTurnsByTeacherIdAndFiltered(teacherId);
  }

  getTurnsByTeacherIdAndFiltered(tId: string): Observable<any> {
    return this.afs.collection('turn', ref => ref.where('teacherId', '==', tId)
      .where('startDate', '>=', moment().subtract(12, 'h').toDate())
      .where('startDate', '<=', moment().add(7, 'd').toDate())).valueChanges();
  }

  getTurnBySearchQuery(queryString: string) {

  }

  createTurn(turnList: any) {
    let error = false;

    turnList.map((turn) => {
      this.afs.collection('turn').add(turn).then((docRef) => {
        docRef.set({ turnId: docRef.id }, { merge: true });
      }).catch((err) => {
        error = true;
      });
    });

    // turnList.forEach(element => {
    //   this.afs.collection('turn').add(element).then(docRef => {
    //     docRef.set({ turnId: docRef.id }, { merge: true });
    //   }).catch((error) => { error = true; console.log(error) });
    // });

    return Promise.resolve(error);
  }

  deleteTurn(turnId): Promise<any> {
    return this.afs.collection('turn').doc(`${turnId}`).delete();
  }
}
